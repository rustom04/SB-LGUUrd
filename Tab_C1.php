<div class="col-lg-12">
    <h3>I. PERSONAL INFORMATION</h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
              <tr>
                    <td>2. SURNAME</td>
                    <td colspan="3"><input type="text" name="surname" id="surname" class="form-control" ></td>
              </tr>
              <tr>
                    <td>FIRST NAME</td>
                    <td><input type="text" name="fname" id="fname" class="form-control" ></td>
                    <td>NAME EXTENSION (JR., SR)</td>
                    <td><input type="text" name="nameext" id="nameext" class="form-control" ></td>
              </tr>
              <tr>
                    <td>MIDDLE NAME</td>
                    <td colspan="3"><input type="text" name="mname" id="mname" class="form-control" ></td>
              </tr>
        </table>
    </div>
</div>

<div class="row">
    <!-- Table COL 2 -->
    <div class="col-lg-6">
        <table class="table table-bordered">
            <tr>
                <td>3. DATE OF BIRTH</td>
                <td class="form-group" width="250">
                    <small>Date Format:&emsp;(mm/dd/yyyy)</small>
                    <input type="date" name="dob" id="dob" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>4. PLACE OF BIRTH</td>
                <td class="form-group">
                    <input type="text" name="pob" id="pob" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>5. SEX</td>
                <td class="form-group">
                    <select name="sex" id="sex" class="form-control" >
                        <option value="">--Select Gender--</option>
                        <option value="MALE">MALE</option>
                        <option value="FEMALE">FEMALE</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>6. CIVIL STATUS</td>
                <td class="form-group">
                    <select name="civil_status" id="civil_status" class="form-control" >
                        <option value="">--Select Status--</option>
                        <option value="SINGLE">SINGLE</option>
                        <option value="MARRIED">MARRIED</option>
                        <option value="WIDOWED">WIDOWED</option>
                        <option value="SEPARATED">SEPARATED</option>
                        <option value="OTHER/S">OTHER/S</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td>7. HEIGHT (m)</td>
                <td class="form-group" width="">
                    <input type="text" name="height" id="height" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>8. WEIGHT (kg)</td>
                <td class="form-group">
                    <input type="text" name="weight" id="weight" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>9. BLOOD TYPE</td>
                <td class="form-group" width="">
                    <input type="text" name="blood_type" id="blood_type" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>10. GSIS ID NO.</td>
                <td class="form-group">
                    <input type="text" name="gsis" id="gsis" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>11. PAG-IBIG NO.</td>
                <td class="form-group" width="">
                    <input type="text" name="pagibig" id="pagibig" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>12. PHILHEALTH NO.</td>
                <td class="form-group">
                    <input type="text" name="philhealth" id="philhealth" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>13. SSS NO.</td>
                <td class="form-group" width="">
                    <input type="text" name="sss" id="sss" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>14. TIN NO.</td>
                <td class="form-group">
                    <input type="text" name="tin" id="tin" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>15. AGENCY EMPLOYEE NO.</td>
                <td class="form-group">
                    <input type="text" name="agency_no" id="agency_no" class="form-control" >
                </td>
            </tr>
        </table>
    </div>

    <!-- Table COL 3 -->
    <div class="col-lg-6">
        <table class="table table-bordered">
            <tr>
                <td rowspan="2" width="150">16. CITIZENSHIP</td>
                <td colspan="3">
                    <div class="form-group">
                      <div class="radio">
                        <label>
                            <input type="radio" name="citizenship" id="citizenship" value="Filipino"  checked="">
                            Filipino
                        </label>
                        &emsp;
                        <label>
                            <input type="radio" name="citizenship" id="citizenship1" value="Dual" >
                            Dual Citizenship
                        </label>
                      </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="input-group-sm">
                    <small>Pls. indicate country:</small>
                    <select name="country" id="country" class="form-control">
                        <option value="">--Select Country--</option>
                        <option value="PHILIPPINES">PHILIPPINES</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td rowspan="3">17. RESIDENTIAL ADDRESS</td>
                <td class="input-group-sm">
                    <small>House/Block/Lot No.</small>
                    <input type="text" name="house_no" id="house_no" class="form-control" >
                </td>
                <td class="input-group-sm" colspan="2">
                    <small>Street</small>
                    <input type="text" name="street" id="street" class="form-control" >
                </td>
            </tr>
            <tr>
                <td class="input-group-sm">
                    <small>Subdivision/Village</small>
                    <input type="text" name="subdivision" id="subdivision" class="form-control" >
                </td>
                <td class="input-group-sm" colspan="2">
                    <small>Barangay</small>
                    <input type="text" name="barangay" id="barangay" class="form-control" >
                </td>
            </tr>
            <tr>
                <td colspan="2" class="input-group-sm">
                    <small>City/Municipality</small>
                    <input type="text" name="city" id="city" class="form-control" >
                </td>
                <td class="input-group-sm" width="300">
                    <small>Province</small>
                    <input type="text" name="province" id="province" class="form-control" >
                </td>
            </tr>
            <tr>
                <td colspan="">
                    ZIP CODE
                </td>
                <td colspan="3" class="input-group-sm">
                    <input type="text" name="zip_code" id="zip_code" class="form-control" >
                </td>
            </tr>




            <tr>
                <td rowspan="3">18. PERMANENT ADDRESS</td>
                <td class="input-group-sm">
                    <small>House/Block/Lot No.</small>
                    <input type="text" name="house_no2" id="house_no2" class="form-control" >
                </td>
                <td class="input-group-sm" colspan="2">
                    <small>Street</small>
                    <input type="text" name="street2" id="street2" class="form-control" >
                </td>
            </tr>
            <tr>
                <td class="input-group-sm">
                    <small>Subdivision/Village</small>
                    <input type="text" name="subdivision2" id="subdivision2" class="form-control" >
                </td>
                <td class="input-group-sm" colspan="2">
                    <small>Barangay</small>
                    <input type="text" name="barangay2" id="barangay2" class="form-control" >
                </td>
            </tr>
            <tr>
                <td colspan="2" class="input-group-sm">
                    <small>City/Municipality</small>
                    <input type="text" name="city2" id="city2" class="form-control" >
                </td>
                <td class="input-group-sm" width="300">
                    <small>Province</small>
                    <input type="text" name="province2" id="province2" class="form-control" >
                </td>
            </tr>
            <tr>
                <td colspan="">
                    ZIP CODE
                </td>
                <td colspan="3" class="input-group-sm">
                    <input type="text" name="zip_code2" id="zip_code2" class="form-control" >
                </td>
            </tr>





            <tr>
                <td class="form-group" width="200">
                    <small>19. TELEPHONE NO.</small>
                    <input type="text" name="telephone" id="telephone" class="form-control" >
                </td>
                <td class="form-group" width="200">
                    <small>20. MOBILE NO.</small>
                    <input type="text" name="mobile" id="mobile" class="form-control" >
                </td>
                <td class="form-group" colspan="2">
                    <small>21. E-MAIL ADDRESS (if any)</small>
                    <input type="text" name="email" id="email" class="form-control" >
                </td>
            </tr>

        </table>
    </div>
    <!-- End of COL 3 -->
</div>




                    


<div class="col-lg-12">
    <h3>II. FAMILY TREE</h3>
</div>
<div class="row">
    <!-- Table COL 4 -->
    <div class="col-lg-6">
        <table class="table table-bordered">
            <tr>
                <td>22. SPOUSE'S SURNAME</td>
                <td class="form-group" width="350" colspan="3">
                    <input type="text" name="height" id="height" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>FIRST NAME</td>
                <td class="form-group">
                    <input type="text" name="weight" id="weight" class="form-control" >
                </td>
                <td><small>NAME EXT (JR.,SR)</small></td>
                <td class="form-group" width="90">
                    <input type="text" name="weight" id="weight" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>MIDDLE NAME</td>
                <td class="form-group" colspan="3">
                    <input type="text" name="blood_type" id="blood_type" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>OCCUPATION</td>
                <td class="form-group" colspan="3">
                    <input type="text" name="gsis" id="gsis" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>EMPLOYER/BUSINESS NAME</td>
                <td class="form-group" colspan="3">
                    <input type="text" name="pagibig" id="pagibig" class="form-control" >
                </td>
            </tr>
            <tr> 
                <td>BUSINESS ADDRESS</td>
                <td class="form-group" colspan="3">
                    <input type="text" name="philhealth" id="philhealth" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>TELEPHONE NO.</td>
                <td class="form-group" colspan="3">
                    <input type="text" name="sss" id="sss" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>24. FATHER'S SURNAME</td>
                <td class="form-group" width="350" colspan="3">
                    <input type="text" name="height" id="height" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>FIRST NAME</td>
                <td class="form-group">
                    <input type="text" name="weight" id="weight" class="form-control" >
                </td>
                <td><small>NAME EXT (JR.,SR)</small></td>
                <td class="form-group" width="90">
                    <input type="text" name="weight" id="weight" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>MIDDLE NAME</td>
                <td class="form-group" colspan="3">
                    <input type="text" name="blood_type" id="blood_type" class="form-control" >
                </td>
            </tr>

            <tr>
                <td colspan="4">25. MOTHER'S MAIDEN NAME</td>
            </tr>
            <tr>
                <td>SURNAME</td>
                <td class="form-group" colspan="3">
                    <input type="text" name="weight" id="weight" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>FIRST NAME</td>
                <td class="form-group" colspan="3">
                    <input type="text" name="weight" id="weight" class="form-control" >
                </td>
            </tr>
            <tr>
                <td>MIDDLE NAME</td>
                <td class="form-group" colspan="3">
                    <input type="text" name="blood_type" id="blood_type" class="form-control" >
                </td>
            </tr>
        </table>
    </div>

    <!-- End of COL 5 -->
    <div class="col-lg-6 table-responsive">
        <table class="table table-bordered" style="font-size: 12px;">
            <thead>
                <tr>
                    <td width="">23. NAME OF CHILDREN <br>(Write full name and list all)</td>
                    <td>DATE OF BIRTH <br>(mm/dd/yyyy)</td>
                    <td>
                        <button type="button" id="add" class="btn btn-primary">
                            <i class="fa fa-plus-square"></i>
                        </button>
                    </td>
                </tr>
            </thead>
            <tbody class="detail">
                <tr>
                    <td>
                        <input type="text" id="textbox8" class="form-control child_name" name="child_name[]">
                    </td>
                    <td>
                        <input type="date" id="textbox9" class="form-control child_dob" name="child_dob[]">
                    </td>
                    <td>
                        <button id="minus" class="remove btn btn-danger"><i class="fa fa-minus-square"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>


    <!-- FOR TABLE NAME OF CHILD-->
    <script type="text/javascript">
        $(function(){
              $('#add').click(function(){
                    addnewrow();
              });

              $('body').delegate('.remove','click',function(){
                    $(this).parent().parent().remove();
              });

        });


        function addnewrow()
        {
            var n = ($('.detail tr').length-0)+1;
            var tr = '<tr>'+
                    '<td><input type="text" class="form-control child_name" name="child_name[]"></td>'+
                    '<td><input type="date" class="form-control child_dob" name="child_dob[]"></td>'+
                    '<td><button id="minus" class="remove btn btn-danger"><i class="fa fa-minus-square"></i></button></td>'+
                    '</tr>';
            $('.detail').append(tr);
        }
    </script>
</div>
                    