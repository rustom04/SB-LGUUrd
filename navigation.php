

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="dashboard.php">Start Bootstrap</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">

        <li class="nav-item">
          <a class="nav-link" href="dashboard.php">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-folder"></i>
            <span class="nav-link-text">Personal Data Sheet</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti">
            <li <?php echo ($page=='PDSCreate' ) ? "class='treeview active'" : ""; ?> >
              <a href="PDSCreate.php">
                <i class="fa fa-fw fa-plus"></i>
                <span>Add Record</span>
              </a>
            </li>
            <li <?php echo ($page=='RecordsView' ) ? "class='treeview active'" : ""; ?>>
              <a href="RecordsView.php">
                <i class="fa fa-fw fa-list"></i>
                <span>View All Records</span>
              </a>
            </li>
          </ul>
        </li>
      </ul>
      
      <?php include('top-menu.php'); ?>

    </div>
  </nav>