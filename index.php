
<?php
@session_start();
include "config/dbconnect.php";

if(@$_SESSION['admin']){
  header("location: dashboard.php");
}else{
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="assets/css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">Login</div>
            <div class="card-body">

                <?php
                    $username = @$_POST['username'];
                    $password = @$_POST['password'];
                    $login = @$_POST['login'];

                    if ($login) {
                    if ($username == "" || $password == "") {
                      ?><script type="text/javascript">alert("Username and Password is incorrect!")</script><?php
                    }else{
                    $login_query = mysqli_query($con,"SELECT * FROM tbl_user WHERE username = '$username' and password = md5('$password') ")or die(mysqli_error());
                    $row = mysqli_num_rows($login_query);
                    $result = mysqli_fetch_array($login_query);


                    if ($row >= 1) {

                        if ($result['user_type'] == "admin") {
                              @$_SESSION['admin'] = $result['id'];
                                //display session into applicant page
                                $_SESSION['id'] = $result['id'];
                                $_SESSION['user_type'] = $result['user_type'];


                        if ($row > 0){
                              session_start();

                              header("location: dashboard.php");

                          }

                        }else{

                        ?>
                        <script type="text/javascript">
                          alert("Invalid Username or Password! Please try again.");
                          window.location.href='index.php'
                        </script>
                        <?php

                        }
                    }else{

                    ?>
                    <script>
                        $(window).load(function(){
                            $('#loginFailed').modal('show');
                            setTimeout(function (){
                            $('#loginFailed').hide().modal('show');
                            });
                        });
                    </script>
                    <?php
                    }
                    }
                    }
                ?>

                <form method="post">
                    <div class="form-group">
                        <label for="">Email address</label>
                        <input class="form-control" id="" type="text" name="username" placeholder="Enter username">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input class="form-control" id="" type="password" name="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <label class="form-check-label">
                            <input class="form-check-input" type="checkbox"> Remember Password</label>
                        </div>
                    </div>
                    <input type="submit" name="login" class="btn btn-primary btn-block" value="Sign In">
                </form>
                <div class="text-center">
                    <a class="d-block small mt-3" href="register.html">Register an Account</a>
                    <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
                </div>
            </div>
        </div>
    </div>
<!-- Bootstrap core JavaScript-->
<script src="asstes/vendor/jquery/jquery.min.js"></script>
<script src="asstes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="asstes/vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>


<?php } ?>