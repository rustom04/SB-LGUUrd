
<!-- Header -->
<?php include('header.php'); ?>
<!-- Navigation-->
<?php $page = 'PDSCreate'; include('navigation.php'); ?>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="index.html">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">PDS | Add Records</li>
    </ol>
    <div class="row">
      <div class="col-12">
        <h1>PDS</h1>
        <p>Personal Data Sheet</p>
      </div>
    </div>

    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-area-chart"></i> Area Chart Example</div>
      <div class="card-body">
        <div id='progress'><div id='progress-complete'></div></div>
        <form id="SignupForm" action="">
            <fieldset>
                <?php include "tab_C1.php"; ?>
            </fieldset>

            <fieldset>
                <?php //include "tab_C2.php"; ?>
            </fieldset>

            <fieldset class="form-horizontal" role="form">
                <legend>Billing information</legend>
                
            </fieldset>
            <p>
                <button id="SaveAccount" class="btn btn-primary submit">Submit</button>
            </p>
        </form>
      </div>
      <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>

  </div>
  <!-- /.container-fluid-->
  <!-- /.content-wrapper-->

  <?php include('footer.php'); ?>

